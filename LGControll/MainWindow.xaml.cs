﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.Xml;

namespace LGControll
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        List<Dictionary<string, string>> chanels;
        string ip;

        public MainWindow()
        {
            InitializeComponent();

            ip = textBoxIp.Text;
            updateChanelsList();
        }

        void showPairingKey()
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create("http://" + ip + ":8080/udap/api/pairing HTTP/1.1");
            request.Method = "POST";

            request.Headers.Add("Cache-Control", "no-cache");
            request.UserAgent = "Windows/6.3 UDAP/2.0 Windows/8.1";
            //request.Connection = "Close";

            string postData = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
            "<envelope>" +
                "<api type=\"pairing\">" +
                    "<name>showKey</name>" +
                "</api>" +
            "</envelope>";
            byte[] byteArray = Encoding.UTF8.GetBytes(postData);


            request.ContentType = "text/xml; charset=utf-8";
            request.ContentLength = byteArray.Length;



            Stream dataStream = request.GetRequestStream();
            dataStream.Write(byteArray, 0, byteArray.Length);
            dataStream.Close();

            // Get the response.
            WebResponse response = request.GetResponse();

            Console.WriteLine(((HttpWebResponse)response).StatusDescription);

            dataStream = response.GetResponseStream();
            StreamReader reader = new StreamReader(dataStream);
            string responseFromServer = reader.ReadToEnd();
            Console.WriteLine(responseFromServer);


            reader.Close();
            dataStream.Close();
            response.Close();
             
        }

        void sentKey()
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create("http://" + ip + ":8080/udap/api/pairing HTTP/1.1");
            request.Method = "POST";

            request.Headers.Add("Cache-Control", "no-cache");
            request.UserAgent = "Windows/6.3 UDAP/2.0 Windows/8.1";
            //request.Connection = "Close";

            string postData = 
                "<?xml version=\"1.0\" encoding=\"utf-8\"?>"+
                "<envelope>" +
                "<api type=\"pairing\">" +
                "<name>hello</name>" +
                "<value>"+textBoxPairingKey.Text+"</value>" +
                "<port>8080</port>" +
                "</api>" +
                "</envelope>";
            byte[] byteArray = Encoding.UTF8.GetBytes(postData);


            request.ContentType = "text/xml; charset=utf-8";
            request.ContentLength = byteArray.Length;



            Stream dataStream = request.GetRequestStream();
            dataStream.Write(byteArray, 0, byteArray.Length);
            dataStream.Close();

            // Get the response.
            WebResponse response = request.GetResponse();

            Console.WriteLine(((HttpWebResponse)response).StatusDescription);

            dataStream = response.GetResponseStream();
            StreamReader reader = new StreamReader(dataStream);
            string responseFromServer = reader.ReadToEnd();
            Console.WriteLine(responseFromServer);


            reader.Close();
            dataStream.Close();
            response.Close();
        }

        

        void sentCommand(string command)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create("http://" + ip + ":8080/udap/api/command HTTP/1.1");
            request.Method = "POST";

            request.Headers.Add("Cache-Control", "no-cache");
            request.UserAgent = "Windows/6.3 UDAP/2.0 Windows/8.1";
            //request.Connection = "Close";

            string postData = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
            "<envelope>" +
            "<api type=\"command\">" +
            "<name>HandleKeyInput</name>" +
            "<value>"+command+"</value>" +
            "</api>" +
            "</envelope>";

            byte[] byteArray = Encoding.UTF8.GetBytes(postData);


            request.ContentType = "text/xml; charset=utf-8";
            request.ContentLength = byteArray.Length;



            Stream dataStream = request.GetRequestStream();
            dataStream.Write(byteArray, 0, byteArray.Length);
            dataStream.Close();

            // Get the response.
            WebResponse response = request.GetResponse();

            Console.WriteLine(((HttpWebResponse)response).StatusDescription);

            dataStream = response.GetResponseStream();
            StreamReader reader = new StreamReader(dataStream);
            string responseFromServer = reader.ReadToEnd();
            Console.WriteLine(responseFromServer);


            reader.Close();
            dataStream.Close();
            response.Close();
        }

        void search()
        {
            
            //IPEndPoint LocalEndPoint = new IPEndPoint(IPAddress.Any, 1900);
            IPEndPoint LocalEndPoint = new IPEndPoint(IPAddress.Any, 3000);
            IPEndPoint MulticastEndPoint = new IPEndPoint(IPAddress.Parse("239.255.255.250"), 1900);

            Socket UdpSocket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);

            UdpSocket.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, true);
            UdpSocket.Bind(LocalEndPoint);
            UdpSocket.SetSocketOption(SocketOptionLevel.IP, SocketOptionName.AddMembership, new MulticastOption(MulticastEndPoint.Address, IPAddress.Any));
            UdpSocket.SetSocketOption(SocketOptionLevel.IP, SocketOptionName.MulticastTimeToLive, 2);
            UdpSocket.SetSocketOption(SocketOptionLevel.IP, SocketOptionName.MulticastLoopback, true);

            Console.WriteLine("UDP-Socket setup done...\r\n");

            //string SearchString = "M-SEARCH * HTTP/1.1\r\nHOST:239.255.255.250:1900\r\nMAN:\"ssdp:discover\"\r\nST:ssdp:all\r\nMX:3\r\n\r\n";


            string SearchString = "" +
            "M-SEARCH * HTTP/1.1\r\n" +
            "HOST: 239.255.255.250:1900\r\n" +
            "MAN: \"ssdp:discover\"\r\n" +
            "USER-AGENT: iOS/5.0 UDAP/2.0 iPhone/4\r\n" +
            "MX: 5\r\n" +
            "ST: urn:schemas-upnp-org:device:MediaRenderer:1\r\n" +
            //"ST:ssdp:all\r\n" +
            "\r\n";

            UdpSocket.SendTo(Encoding.UTF8.GetBytes(SearchString), SocketFlags.None, MulticastEndPoint);

            Console.WriteLine("M-Search sent...\r\n");

            byte[] ReceiveBuffer = new byte[64000];

            int ReceivedBytes = 0;

            while (true)
            {
                if (UdpSocket.Available > 0)
                {
                    ReceivedBytes = UdpSocket.Receive(ReceiveBuffer, SocketFlags.None);

                    if (ReceivedBytes > 0)
                    {
                        Console.WriteLine(Encoding.UTF8.GetString(ReceiveBuffer, 0, ReceivedBytes));
                        break;
                    }
                }
            }
           
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            showPairingKey();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            sentKey();
        }

        private void buttonVolUp_Click(object sender, RoutedEventArgs e)
        {
            sentCommand("24");
        }

        private void buttonVolDown_Click(object sender, RoutedEventArgs e)
        {
            sentCommand("25");
        }

        private void buttonMute_Click(object sender, RoutedEventArgs e)
        {
            sentCommand("26");
        }

        void getCurrentChannel()
        {
            string sURL = "http://" + ip + ":8080/udap/api/data?target=cur_channel";

            HttpWebRequest wrGETURL = (HttpWebRequest)WebRequest.Create(sURL);

            wrGETURL.UserAgent = "Windows/6.3 UDAP/2.0 Windows/8.1";
            wrGETURL.Method = "GET";
            wrGETURL.Headers.Add("Cache-Control", "no-cache");

            Stream objStream = wrGETURL.GetResponse().GetResponseStream();

            StreamReader objReader = new StreamReader(objStream);

            string sLine = "";
            int i = 0;

            while (sLine != null)
            {
                i++;
                sLine = objReader.ReadLine();
                if (sLine != null)
                    Console.WriteLine("{0}", sLine);
            }
            Console.ReadLine();
        }

        void setChanel(string major, string minor, string sorceIndex, string physicalNum)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create("http://" + ip + ":8080/udap/api/command HTTP/1.1");
            request.Method = "POST";

            request.Headers.Add("Cache-Control", "no-cache");
            request.UserAgent = "Windows/6.3 UDAP/2.0 Windows/8.1";
            //request.Connection = "Close";

            string postData = 

            "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
                "<envelope>" +
                    "<api type=\"command\">" +
                        "<name>HandleChannelChange</name>" +
                        "<major>" + major + "</major>" +
                        "<minor>" + minor + "</minor>" +
                        "<sourceIndex>" + sorceIndex + "</sourceIndex>" +
                        "<physicalNum>" + physicalNum + "</physicalNum>" +
                    "</api>" +
                "</envelope>";
 

            byte[] byteArray = Encoding.UTF8.GetBytes(postData);


            request.ContentType = "text/xml; charset=utf-8";
            request.ContentLength = byteArray.Length;



            Stream dataStream = request.GetRequestStream();
            dataStream.Write(byteArray, 0, byteArray.Length);
            dataStream.Close();

            // Get the response.
            WebResponse response = request.GetResponse();

            Console.WriteLine(((HttpWebResponse)response).StatusDescription);

            dataStream = response.GetResponseStream();
            StreamReader reader = new StreamReader(dataStream);
            string responseFromServer = reader.ReadToEnd();
            Console.WriteLine(responseFromServer);


            reader.Close();
            dataStream.Close();
            response.Close();
        }

        void listenSocket()
        {
            
            TcpListener serverSocket = new TcpListener(8080);
            int requestCount = 0;
            TcpClient clientSocket = default(TcpClient);
            serverSocket.Start();
            Console.WriteLine(" >> Server Started");
            clientSocket = serverSocket.AcceptTcpClient();
            Console.WriteLine(" >> Accept connection from client");
            requestCount = 0;

            while ((true))
            {
                try
                {
                    requestCount = requestCount + 1;
                    NetworkStream networkStream = clientSocket.GetStream();
                    byte[] bytesFrom = new byte[10025];
                    networkStream.Read(bytesFrom, 0, (int)clientSocket.ReceiveBufferSize);
                    string dataFromClient = System.Text.Encoding.ASCII.GetString(bytesFrom);
                    dataFromClient = dataFromClient.Substring(0, dataFromClient.IndexOf("$"));
                    Console.WriteLine(" >> Data from client - " + dataFromClient);
                    string serverResponse = "Last Message from client" + dataFromClient;
                    Byte[] sendBytes = Encoding.ASCII.GetBytes(serverResponse);
                    networkStream.Write(sendBytes, 0, sendBytes.Length);
                    networkStream.Flush();
                    Console.WriteLine(" >> " + serverResponse);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }
            }

            clientSocket.Close();
            serverSocket.Stop();
            Console.WriteLine(" >> exit");
            Console.ReadLine();

        }

        string getChannelList()
        {
            string sURL = "http://" + ip + ":8080/udap/api/data?target=channel_list";

            HttpWebRequest wrGETURL = (HttpWebRequest)WebRequest.Create(sURL);

            wrGETURL.UserAgent = "Windows/6.3 UDAP/2.0 Windows/8.1";
            wrGETURL.Method = "GET";
            wrGETURL.Headers.Add("Cache-Control", "no-cache");

            Stream objStream = wrGETURL.GetResponse().GetResponseStream();

            StreamReader objReader = new StreamReader(objStream);

            string sLine = "";
            int i = 0;

            string result = "";
            while (sLine != null)
            {
                i++;
                sLine = objReader.ReadLine();
                if (sLine != null)
                {
                    result += sLine;
                    //Console.WriteLine("{0}", sLine);
                }
            }
            return result;
        }

        private void butonGetChanel_Click(object sender, RoutedEventArgs e)
        {
            getCurrentChannel();
        }

        private void buttonGetAllChannels_Click(object sender, RoutedEventArgs e)
        {
            updateChanelsList();
        }

        void updateChanelsList()
        {
            string channelsXml = getChannelList();
            chanels = parseXML(channelsXml);
            fillChanelsList(chanels);
        }

        List<Dictionary<string, string>> parseXML(string xml)
        {
            List<Dictionary<string, string>> chanels = new List<Dictionary<string, string>>();

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xml);
            XmlNode node = doc.SelectSingleNode("envelope").SelectSingleNode("dataList");
            foreach(XmlNode chanelNode in node.SelectNodes("data"))
            {
                string major = chanelNode.SelectSingleNode("major").InnerText;
                string minor = chanelNode.SelectSingleNode("minor").InnerText;
                string sourceIndex = chanelNode.SelectSingleNode("sourceIndex").InnerText;
                string physicalNum = chanelNode.SelectSingleNode("physicalNum").InnerText;
                string chname = chanelNode.SelectSingleNode("chname").InnerText;

                Dictionary<string, string> chanel = new Dictionary<string, string>();
                chanel.Add("major", major);
                chanel.Add("minor", minor);
                chanel.Add("sourceIndex", sourceIndex);
                chanel.Add("physicalNum", physicalNum);
                chanel.Add("chname", chname);

                chanels.Add(chanel);

                /*
                Console.WriteLine("name: " + chname);
                Console.WriteLine("major: " + major);
                Console.WriteLine("minor: " + minor);
                Console.WriteLine("sourceIndex: " + sourceIndex);
                Console.WriteLine("physicalNum: " + physicalNum);
                Console.WriteLine();
                 */

            }

            return chanels;
        }

        void fillChanelsList(List<Dictionary<string, string>> chanels)
        {
            listViewChannels.Items.Clear();
            foreach( Dictionary<string,string> chanel in chanels)
            {
                ListData data = new ListData { Name = chanel["chname"], Chanel = chanel["major"] };
                listViewChannels.Items.Add(data);
            }

        }

        public class ListData
        {
            public string Name { get; set; }
            public string Chanel { get; set; }
        }

        private void ListViewItem_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            ListViewItem item = sender as ListViewItem;

            var itemSender = (sender as FrameworkElement).DataContext;
            int index = listViewChannels.Items.IndexOf(itemSender);

            if(index >= 0)
            {
                Dictionary<string, string> chanel = chanels[index];
                setChanel(chanel["major"], chanel["minor"], chanel["sourceIndex"], chanel["physicalNum"]);
            }
        }
    }
}
